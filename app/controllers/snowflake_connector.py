import snowflake.connector

from app import main as main


def make_connection(user=None, 
					password=None, 
					account=None, 
					database=None, 
					warehouse=None):
	"""Function to create the connection to Snowflake
	
		In order to access the Snowflake data, we need to establish a connection.
		Since we store all the relevant connection information in the apps 
		config, we can just grab it from there to make the connection, but we also
		provide the option to supply the parameters. As an aside, we don't give
		the option to change the paramstyle

		Parameters:
			user (str) - Optional; The user to use for connecting to Snowflake
			password (str) - Optional; The password to use for connecting to
				Snowflake
			account (str) - Optional; The account to use for connecting to 
				Snowflake
			database (str) - Optional; The database to connect to in Snowflake
			warehouse (str) - Optional; The warehouse to grab the data from in 
				Snowflake

		Returns:
			A snowflake connector Connection object 

	"""
	config = main.app.config
	user = user if user is not None else config['SNOWFLAKE_USERNAME']
	password = password if password is not None else config['SNOWFLAKE_PASSWORD']
	database = database if database is not None else config['SNOWFLAKE_DATABASE']
	warehouse = warehouse if warehouse is not None else config['SNOWFLAKE_WAREHOUSE']
	account = account if account is not None else config['SNOWFLAKE_ACCOUNT']

	try:
		conn = snowflake.connector.connect(
			user = user, 
			password = password,
			account = account,
			database = database,
			warehouse = warehouse,
			paramstyle = 'qmark'
		)
	except Exception:
		conn = None

	return conn

def execute_query(query, parameters=None, connection=None):
	"""Function to execute the query against the connection

		After we have a connection we need a way to make queries against the 
		relevant data. We split the parameters from the query to prevent any
		form of injection a la little bobby drop tables
	
		Parameters:
			query (str) - The base query to execute
			parameters (dict) - Optional; A dictionary containing the parameters 
				where the key is the parameter namn and the value is the parameter 
				value
			connection (Snowflake connection) - Optional; Since make_connection
				can accept various other parameters and we don't want to have to
				always pass those parameters in, we can just accept a previously
				established connection object here
	
		Returns:
			Tuple (Boolean, Object) - 
				Boolean - The pass/fail status of the request. True = Pass
				Object - A list of rows (in tuple form) of the requested data OR 
					the error message if first value is False

	"""
	try:
		conn = connection if connection else make_connection()
		cursor = conn.cursor()
		t_params = list()
		if parameters is not None:
			query += ' WHERE'
			for key in parameters.keys():
				query += f' {key}=?'
				t_params.append(parameters[key])
				if key != list(parameters.keys())[-1]:
					query += ' AND'
		t_params = tuple(t_params)
		cursor.execute(query, t_params)
		rows = cursor.fetchall()

		return (True, rows)
	except Exception as e:
		return (False, str(e))
	finally:
		if not connection:
			conn.close()

def execute_stored_procedure(proc_name, parameters=None, database_name=None):
	"""Function used to execute a saved stored procedure

	Snowflake is pretty powerful in its ability to process data, so we need a way
	to execute stored procedures that have been created to speed up data 
	retrieval. This function exists to call the stored procedures with supplied
	parameters

	Parameters
		proc_name (str) - The name of the stored procedure to call
		parameters (list[str]) - Optional; The parameters to pass along with the
			stored procedure
		database_name (str) - Optional; Currently, there is no mapping from 
			procedures to database locations. 

	Returns
		Tuple (Boolean, List)
			Boolean - True on success, Fail on error
			Object - A list of rows (in tuple form) of the requested data OR
				the error message

	"""
	try:
		if parameters is None:
			parameters = list()
	
		full_proc = f'CALL {proc_name}({",".join(parameters)})'
		with make_connection(database=database_name) as conn:
			cursor = conn.cursor()
			cursor.execute(full_proc)

			rows = cursor.fetchall()
			return (True, rows)
	except Exception as e:
		return (False, str(e))
