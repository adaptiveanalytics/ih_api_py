import os

from datetime import datetime

import firebase_admin
from firebase_admin import credentials
from firebase_admin import firestore

from app import main as main

def get_credentials_object():
	"""Function to get the credentials object for Firebase

	The first step in firebase connection is to ensure a valid credentials
	object. We provide the flexibility to call with different objects,
	although this should never be needed.

	Returns:
		firebase_admin.Certificate object based on the supplied creds file

	"""
	
	app_config = main.app.config
	file_path = os.sep.join(__file__.split(os.sep)[:-1])
	if os.sep not in app_config['FIREBASE_JSON']:
		path_to_cred = os.path.join(file_path, app_config['FIREBASE_JSON'])
	cred_obj = credentials.Certificate(path_to_cred)

	return cred_obj

def create_app(app_name=None, bucket_name='xplor-insights'):
	"""Function to create a firebase connection app

	This function will create an app that can be used to execute queries and 
	get data from Firebase. Since multiple apps can be supported in one running
	we provide the option for a user provided name. If one is not provided, 
	a timestamp will be used

	Parameters
		app_name (str) - Optional; the name to setup the app with.

	Returns
		str - The app name that was created. Will be None if app init failed
	"""

	try:
		app_name = app_name if app_name is not None else datetime.now().strftime('%Y-%m-%dT%H%M%S')
		creds_obj = get_credentials_object()

		firebase_admin.initialize_app(
			credential=creds_obj, 
			options={
				'storageBucket' : f'https://{bucket_name}.appspot.com'
			},
			name=app_name
		)
	except ValueError:
		app_name = None

	return app_name

def get_app_instance(app_name):
	"""Function to retrieve an app instance that can be used for connections

	This function is primarily a wrapper to the get app function of firebase
	admin, but it allows us to pass around app names instead of the app 
	itself. This allows us to exit gracefully as well.

	Parameters
		app_name (str) - The name of the app whose instance needs retrieved

	Returns
		An App instance from firebase admin
	"""
	try:
		app = firebase_admin.get_app(app_name)
	except ValueError:
		app = None

	return app

def teardown_app(app_name):
	"""Function used to delete an open app instance

	There can only be a number of apps open with the same name. While this 
	isn't strictly a problem when we name them with timestamps, if one or 
	more is attempted to be opened with the same name, we don't like that.
	Not to mention, keeping an app open is just bad practice and could 
	probably get expensive eventually

	Parameters
		app_name (str) - The name of the app to disconnect

	Returns
		Boolean - True if app was deleted, false if app was never init	

	"""

	try:
		app = get_app_instance(app_name)
		if not app:
			return False
		firebase_admin.delete_app(app)
		return True
	except ValueError:
		return False

def get_firestore_client(app_name):
	"""Function to get the Firestore client that contains data

	This is a wrapper around the Firestore client object so that
	we can actually grab the data. Will return None if the app
	has not been initialized properly

	Parameters
		app_name (str) - The name of the initialized app

	Returns 
		The Firestore client, or None if app is not properly init
	"""
	app = get_app_instance(app_name)
	if not app:
		return None
	client = firestore.client(app)

	return client

def get_data(collection, document):
	"""Function to get the dictionary data of the supplied document

	This function is used to actually extract the data from the requested 
	document within the requested collection. Will return None if the doc or 
	collection is not found.

	Parameters
		collection (str) - The collection in which to search for the document
		document (str) - The name of the document to search for

	Returns
		dict - The document keys and values as a dictionary.
	"""
	app_name = create_app()
	client = get_firestore_client(app_name)
	if not client:
		return None
	doc_ref = client.collection(collection).document(document)
	data = doc_ref.get().to_dict()
	teardown_app(app_name)

	return data