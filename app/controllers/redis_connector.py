import os

import redis

from app import main as main

def create_connection(host=None, port=None, password=None):
	"""Function to create a connection to redis

	This function simply wraps the connection creation. You can supply the 
	connection parameters. If not supplied it will use the default from the 
	app config.

	Redis will allow this to succeed even if it doesn't have anything to
	connect to, so the error will occur on the get/set methods. No need to do 
	any checking or exception handling here :shrug:

	Parameters:
		host(str) - Optional; the host running the Redis instance to connect to
		port(int) - Optional; the port on which the Redis instance is running
			on the host machine
		password(str) - Optional; the password for the Redis instance to 
			connect to

	Returns:
		A Redis object that can be used to do things.

	"""
	config = main.app.config

	host = host if host else config['REDIS_HOST']
	port = port if port else config['REDIS_PORT']
	password = password if password else config['REDIS_PASSWORD']

	redis_conn = redis.Redis(
		host = host,
		port = port,
		password = password)

	return redis_conn

def add_data(
	redis_connection, 
	data_key, 
	data_value,
	timeout_in_milliseconds=None,
	timeout_in_seconds=None,
	only_set_if_exists=False,
	only_set_if_nonexistant=False,
	return_previous_value=False
	):
	"""Function to add data to the redis db

	Redis stores data in a key-value pairing. It tends to be faster than native
	Python Dictionary storage, and allows for all kinds of things to be stored.
	This function is used to simply add data to the Redis cache. It's a wrapper
	for the "set" function. The reason it's being wrapped is in case we want
	to add some custom functionality in the future.

	A few notes:
		- If both timeouts are specified, both will be passed. See Redis
		documentation to find out what happens in that case
		-If both "only_set" options are set to True, this function will throw
		an exception. Don't be dumb

	Parameters:
		redis_connection(redis.Redis) - The redis instance connection to add
			the data to.
		data_key (Object) - The key to store the data under
		data_value (Object) - The value to store in the data
		timeout_in_milliseconds (int) - Optional; The timout (in milliseconds) 
			for the key. 
		timeout_in_seconds (int) - Optional; The timeout (in seconds) for the 
			key
		only_set_if_exists (bool) - Optional; Flag to only set a value in the 
			key if it already exists. 
		only_set_if_nonexistant (bool) - Optional; Flag to only set a value 
			if the key didn't previously exist
		return_previous_value (bool) - Optional; Returns the value previously
			stored by the key. 

	Returns:
		(bool, Object) - 
			bool - True if operation was successful, False otherwise
			Object - Three possibilities here
				1) None if operation was a success and return_previous_value 
					was set to False
				2) Previous value contained in the key if operation was a 
					success and return_previous_value was set to True
				3) Exception text if an exception was thrown

	"""
	if only_set_if_exists and only_set_if_nonexistant:
		raise ValueError('only_set_if_exists and only_set_if_nonexistant are mutually exclusive, both cannot be set to True')
	try:
		result = redis_connection.set(
			name = data_key, 
			value = data_value,
			ex = timeout_in_seconds,
			px = timeout_in_milliseconds,
			nx = only_set_if_nonexistant,
			xx = only_set_if_exists,
			get = return_previous_value
		)

		result_text = None if not return_previous_value else result
		
		return (True, result_text)
	except Exception as e:
		print(e)
		return (False, str(e))

def get_data(redis_connection, data_key):
	"""Function to get the data stored by a key

	This is a wrapper of the built-in get function. The reason for wrapping is
	simply to allow flexibility of data handling. 

	Parameters:
		redis_connection(redis.Redis) - The connection to pull the data from
		data_key (str) - The key storing the data

	Returns:
		(bool, Object) - 
			bool - True if operation was successful, False otherwise
			Object - Three possibilities here
				1) None if the key did not exist
				2) The value retrieved if the key existed
				3) Exception text if an exception was thrown

	"""
	try:
		value = redis_connection.get(data_key)
		return (True, value)
	except Exception as e:
		print(e)
		return (False, str(e))