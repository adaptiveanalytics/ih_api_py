from app.main import app as application

if __name__ == '__main__':
	port = int(os.environ.get('PORT', 90))
	application.run(host='0.0.0.0', port=port)