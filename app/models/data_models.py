def model_data(data, template):
	"""Function used to model data based on the supplied template

	This function currently exists as a placeholder. Eventually, 
	a template name will be passed in along side the data that will determine
	how the data should be shaped.

	Parameters:
		data (dict/list) - A json like object retrieved from calling a 
			stored proc. 
		template (str) - The name of the template used to model the 
			data.

	Returns:
		(dict / list) - The data that has been modeled as requested
			by the template.

	"""
	return data