import os
import sys
import threading

from flask import Flask, request, abort
from flask_restx import Api, Resource

sys.path.append('.')
sys.path.append('..')

import app.config as config
import app.controllers.snowflake_connector as snow
import app.controllers.firebase_controller as fire
from  app.models.data_models import model_data
import app.controllers.redis_connector as redis


app = Flask(__name__)
api = Api(app)

app.config.from_object(config.env_config[os.getenv('FLASK_ENV', 'development')])


WHITELIST = list()
fire.create_app(app_name='updater_app')
FB_CLIENT = fire.get_firestore_client('updater_app')
WHITELIST_DOC = FB_CLIENT.collection('shared_config').document('insights_data_server')

def on_snapshot(col_snapshot, changes, read_time):
	global WHITELIST

	for change in changes:
		change_dict = change.document.to_dict()
		if 'whiteListIps' in change_dict:
			WHITELIST = change_dict['whiteListIps']
	print(WHITELIST)

threading.Thread(target=WHITELIST_DOC.on_snapshot(on_snapshot))


@app.before_request
def whitelist():
	if request.remote_addr not in WHITELIST:
		abort(403)

@api.route('/dummy')
class DummyRoute(Resource):
	def get(self):
		return {'Message' : 'Success'}, 200
	def post(self):
		try:
			request_data = request.json
		except Exception:
			return {'Error' : 'Missing request data'}, 400

		return request_data, 200
	
	def put(self):
		return {'Error' : 'Method not allowed'}, 405
	def delete(self):
		return {'Error' : 'Method not allowed'}, 405

@api.route('/stored_proc')
class StoredProcs(Resource):
	def post(self):
		request_data = request.json

		if 'Procedure' not in request_data:
			return {'Error' : 'Missing required data'}, 400

		proc_name = request_data.get('Procedure')
		params = request_data.get('Parameters')
		
		database = request_data.get('Database') if 'Database' in request_data else None
		template = request_data.get('Template') if 'Template' in request_data else None

		result = snow.execute_stored_procedure(proc_name,params, database)

		if result[0]:
			return model_data(result[1], template), 200
		return {'Error' : 'A server error has occurred'}, 500


	def get(self):
		return {'Error' : 'Method not allowed'}, 405
	def put(self):
		return {'Error' : 'Method not allowed'}, 405
	def delete(self):
		return {'Error' : 'Method not allowed'}, 405

if __name__ == '__main__':
	port = int(os.environ.get('PORT', 5000))
	app.run(debug=True, host='0.0.0.0', port=port)