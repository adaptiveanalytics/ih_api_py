import os

basdir = os.path.abspath(os.path.dirname(__file__))

class Config:
	SNOWFLAKE_ACCOUNT = 'THA72227'
	SNOWFLAKE_DATABASE = 'AADATAWAREHOUSE'
	SNOWFLAKE_DM_DATABASE = 'AAMBODATAMART'
	SNOWFLAKE_WAREHOUSE = 'COMPUTE_AA_API'
	SNOWFLAKE_PASSWORD = 'Adaptive2018!'
	SNOWFLAKE_USERNAME = 'adaptiveanalytics'

	FIREBASE_JSON = 'firebase_key.json'

	REDIS_HOST = '10.10.0.4'
	REDIS_PORT = 6379
	REDIS_PASSWORD = os.environ.get('REDIS_PASSWORD')

	@staticmethod
	def init_app(app):
		pass

class DevConfig(Config):
	DEBUG = True
	SNOWFLAKE_DATABASE = 'DEV_AADATAWAREHOUSE'
	SNOWFLAKE_WAREHOUSE = 'COMPUTE_AA_WAREHOUSE_DEV'

class QAConfig(Config):
	TESTING = True
	SNOWFLAKE_DATABASE = 'DEV_AADATAWAREHOUSE'
	SNOWFLAKE_WAREHOUSE = 'COMPUTE_AA_WAREHOUSE_DEV'

class StagingConfig(Config):
	SNOWFLAKE_DATABASE = 'STAGING_AADATAWAREHOUSE'
	SNOWFLAKE_WAREHOUSE = 'COMPUTE_AA_WAREHOUSE_STAGING'

class ProdConfig(Config):
	SNOWFLAKE_DATABASE = 'AADATAWAREHOUSE'
	SNOWFLAKE_WAREHOUSE = 'COMPUTE_AA_API'

env_config = {
	'development' : DevConfig,
	'testing'	  : QAConfig,
	'staging'	  : StagingConfig,
	'production'  : ProdConfig
}
