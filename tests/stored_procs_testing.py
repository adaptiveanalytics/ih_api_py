import unittest
import sys

sys.path.append('.')
sys.path.append('..')

from app.main import app

class StoredProcedureTesting(unittest.TestCase):

	def setUp(self):
		self.app = app
		self.client = app.test_client()

	def testStoredProcGetExpectedFail_MethodNotAllowed(self):
		expectedData = {'Error' : 'Method not allowed'}

		response = self.client.get('/stored_proc')
		responseJSON = response.json

		self.assertEqual(response.status_code, 405)
		self.assertEqual(expectedData, responseJSON)

	def testStoredProcPutExpectedFail_MethodNotAllowed(self):
		expectedData = {'Error' : 'Method not allowed'}

		response = self.client.put('/stored_proc')
		responseJSON = response.json

		self.assertEqual(response.status_code, 405)
		self.assertEqual(expectedData, responseJSON)

	def testStoredProcDeleteExpectedFail_MethodNotAllowed(self):
		expectedData = {'Error' : 'Method not allowed'}

		response = self.client.delete('/stored_proc')
		responseJSON = response.json

		self.assertEqual(response.status_code, 405)
		self.assertEqual(expectedData, responseJSON)

	def testStoredProcPostExpectedFail_MissingProcedureName(self):
		expectedData = {'Error' : 'Missing required data'}
		sendData = {}

		response = self.client.post('/stored_proc', json=sendData)
		responseJSON = response.json

		self.assertEqual(response.status_code, 400)
		self.assertEqual(expectedData, responseJSON)