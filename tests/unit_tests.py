import os
import sys
import time
import unittest

from datetime import datetime

import app.controllers.firebase_controller as firebase
import app.controllers.snowflake_connector as snowflake
import app.models.data_models as models
import app.controllers.redis_connector as redis

from app.main import app

sys.path.append('.')
sys.path.append('..')

class FirebaseTester(unittest.TestCase):
	@classmethod
	def setUpClass(cls):
		os.environ['FLASK_ENV'] = 'development'

	@classmethod
	def tearDownClass(cls):
		del os.environ['FLASK_ENV']

	def setUp(self):
		self.app_name = 'TestApp'
		created_name = firebase.create_app(self.app_name)

		self.assertEqual(self.app_name, created_name)

	def tearDown(self):
		teardown_result = firebase.teardown_app(self.app_name)

		self.assertTrue(teardown_result) 


	def testCreateApp_ExpectedFail_MultipleAppsSameName(self):
		app_name = 'TestApp'

		test_app = firebase.create_app(app_name)
		
		self.assertIsNone(test_app)

	def testRetrieveAppInstance_ExpectedPass(self):
		app_instance = firebase.get_app_instance(self.app_name)

		self.assertIsNotNone(app_instance)

	def testRetrieveAppInstance_ExpectedFail_InvalidName(self):
		app_instance = firebase.get_app_instance('ThisReallyShouldFail')

		self.assertIsNone(app_instance)

	def testRetrieveClient_ExpectedSuccess(self):
		client = firebase.get_firestore_client(self.app_name)

		self.assertIsNotNone(client)

	def testRetrieveClient_ExpectedFail(self):
		client = firebase.get_firestore_client('ThisReallyShouldFailAsWell')

		self.assertIsNone(client)

	def testDataRetrieval_ExpectedSuccess(self):
		expected_data = {'Data' : 'Value', 'More Data' : 'More Value'}
		collection = 'testing'
		document = 'test-blob'

		data = firebase.get_data(collection, document)

		self.assertEqual(expected_data, data)

	def testDataRetrieval_ExpectedFailure_InvalidCollection(self):
		collection = 'ThisBetterBeInvalid'
		document = 'test-blob'

		data = firebase.get_data(collection, document)

		self.assertIsNone(data)

	def testDataRetrieval_ExpectedFailure_InvalidDocument(self):
		collection = 'testing'
		document = 'ThisAlsoBetterBeInvalid'

		data = firebase.get_data(collection, document)

		self.assertIsNone(data)



class SnowflakeTester(unittest.TestCase):
	@classmethod
	def setUpClass(cls):
		os.environ['FLASK_ENV'] = 'development'
		
	@classmethod
	def tearDownClass(cls):
		del os.environ['FLASK_ENV']

	def setUp(self):
		self.connection = snowflake.make_connection()

		self.assertIsNotNone(self.connection)

	def tearDown(self):
		self.connection.close()

	def testExecuteQuery_ExpectedSuccess(self):
		expected_data = ('ShawnTesting', 'AdminUser', 'shawntracy@gmail.com', '97020', 'CMFree', 60707)
		query = 'SELECT * FROM AA.CD_USERS'
		params = {'FNAME' : 'ShawnTesting'}

		response = snowflake.execute_query(query, params, self.connection)

		self.assertTrue(response[0])
		for i in range(len(response[1][0])):
			self.assertEqual(expected_data[i], response[1][0][i])

	def testExecuteQuery_ExpectedFailure_InvalidQuery(self):
		query = 'SELECT'

		response = snowflake.execute_query(query)

		self.assertFalse(response[0])

	def testExecuteQuery_ExpectedFailure_InvalidConnection(self):
		query = 'SELECT * FROM AA.CD_USERS'
		params = {'FNAME' : 'ShawnTesting'}

		response = snowflake.execute_query(query, params, 'SomeConnection')

		self.assertFalse(response[0])

class DataModelsTester(unittest.TestCase):
	# Currently nothing being done with data modeling, so this is blank
	@classmethod
	def setUpClass(cls):
		os.environ['FLASK_ENV'] = 'development'
		
	@classmethod
	def tearDownClass(cls):
		del os.environ['FLASK_ENV']
