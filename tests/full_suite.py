import os
import sys
import unittest

import dummy_testing
import stored_procs_testing
import unit_tests


def suite():
	test_suite = unittest.TestSuite()

	test_suite.addTest(unittest.makeSuite(dummy_testing.DummyTesting))
	test_suite.addTest(unittest.makeSuite(stored_procs_testing.StoredProcedureTesting))
	test_suite.addTest(unittest.makeSuite(unit_tests.FirebaseTester))
	test_suite.addTest(unittest.makeSuite(unit_tests.SnowflakeTester))
	test_suite.addTest(unittest.makeSuite(unit_tests.DataModelsTester))

	return test_suite

if __name__ == '__main__':
	unittest.defaultTestLoader.sortMethodsUsing = None
	run_suite = suite()
	suite_runner = unittest.TextTestRunner(verbosity=2)
	suite_runner.run(run_suite)