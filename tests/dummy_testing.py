import unittest
import sys

sys.path.append('.')
sys.path.append('..')

from app.main import app

class DummyTesting(unittest.TestCase):

	def setUp(self):
		self.app = app
		self.client = app.test_client()

	def testDummyGetExpectedSuccess(self):
		expectedData = {'Message' : 'Success'}

		response = self.client.get('/dummy')
		responseJSON = response.json

		self.assertEqual(response.status_code, 200)
		self.assertEqual(responseJSON, expectedData)

	def testDummyPostExpectedSuccess(self):
		sendData = {"Hello" : "World"}

		response = self.client.post('/dummy', json=sendData)
		responseJSON = response.json

		self.assertEqual(response.status_code, 200)
		self.assertEqual(sendData, responseJSON)

	def testDummyPostExpectedFailMissingData(self):
		headers = {'Content-Type' : 'application/json'}
		expectedData = {'Error' : 'Missing request data'}

		response = self.client.post('/dummy', headers=headers)
		responseJSON = response.json

		self.assertEqual(response.status_code, 400)
		self.assertEqual(expectedData, responseJSON)


	def testDummyPutExpectedFail(self):
		expectedData = {'Error' : 'Method not allowed'}

		response = self.client.put('/dummy')
		responseJSON = response.json

		self.assertEqual(response.status_code, 405)
		self.assertEqual(expectedData, responseJSON)

	def testDummyDeleteExpectedFail(self):
		expectedData = {'Error' : 'Method not allowed'}
		
		response = self.client.delete('/dummy')
		responseJSON = response.json

		self.assertEqual(response.status_code, 405)
		self.assertEqual(expectedData, responseJSON)