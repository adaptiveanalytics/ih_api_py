FROM python:3.10-slim-buster AS build

SHELL ["/bin/bash", "-c"] 

WORKDIR /usr/src/app

COPY . .

RUN apt-get update \
	&& apt-get install gcc -y \
	&& apt-get install uwsgi -y \
	&& apt-get install uwsgi-plugin-python3 -y \
	&& apt-get install curl -y \
	&& apt-get clean

ENV DD_AGENT_MAJOR_VERSION=7 
ENV DD_API_KEY=241680f8c6eb39dcca7b7f36f2c99347
RUN bash -c "$(curl -L https://s3.amazonaws.com/dd-agent/scripts/install_script.sh)"

RUN python -m pip install --upgrade pip
RUN python -m pip install -r ./requirements.txt
RUN python -m pip install "uWSGI==2.0.20"
RUN python -m pip install ddtrace

ENV FLASK_ENV=development
ENV DD_LOGS_INJECTION=true
ENV DD_PROFILING_ENABLED=true
ENV DD_APM_ENABLED=true


EXPOSE 90
EXPOSE 8126
EXPOSE 5001

LABEL "com.datadoghq.ad.check_names"='["redisdb"]'
LABEL "com.datadoghq.ad.init_configs"='[{}]'
LABEL "com.datadoghq.ad.instances"='[{"host":"10.10.0.4","port":"6379","password":"%%env_REDIS_PASSWORD%%"}]'

CMD ["uwsgi", "--ini", "./uwsgi_config.ini" ]