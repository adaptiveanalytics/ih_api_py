# New API Code based around Python

## Requirements
* [Python 3.6+](https://www.python.org/downloads/)
* [Docker Desktop](https://www.docker.com/products/docker-desktop)
---
## Launching Locally
* Create a virtual environment
	* ```py -m venv env```
* Activate the virtual environment
	* Windows
	    * ```env\Scripts\activate.bat```
	* Linux  
	    * ```source env/Scripts/activate```
* Install dependencies
    * ```pip install -r requirements.txt```
* Set FLASK_ENV to appropriate option
	* Windows 
	    * ```set FLASK_ENV=development```
	* Linux   
	    * ```export FLASK_ENV=development```
* Launch the app/main.py file
    * ```py app/main.py```

## Launching with Docker
* On first launch, the image must be built
    * ```docker-compose up --build```
* On subsequent launches, the image can just be launched
    * ```docker-compose up```
* To stop the container, simply send SIGINT (Ctrl-C / Cmd-C)
---
## Whitelist
The api app comes with an app level whitelist that will be suplemented with a machine level whitelist once deployed. 
The whitelist is automatically enforced, to add or remove IP's from the whitelist simply modify the whitelist file